require("./wasm_exec")
const initializeWasm = require("./wasm.js.loader").initializeWasm

const fs = require("fs")

const wasmFile = fs.readFileSync("./main.wasm")

initializeWasm(wasmFile).then(result => {
  console.log(Handle({
    firstName: "Bob",
    lastName: "Morane"
  }))
})
