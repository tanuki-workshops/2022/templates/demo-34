package main

import (
	"fmt"
	"testing"
)

// GOOS=js GOARCH=wasm go test -exec="$(go env GOROOT)/misc/wasm/go_js_wasm_exec"

func TestSayHello(t *testing.T) {
	
	result := SayHello()
	fmt.Println(result)

	expected_result := "hello"

	if result != expected_result {
		t.Error("Incorrect result, expected '"+ expected_result +"', got", result)
	}
}
