# simple function

Once the project created on GitLab.com (or on a GitLab self-managed instance), you can clone it on your own computer:

```bash
#Create a new repository
git clone git@gitlab.com:tanuki-workshops/2022/demo.git
cd demo
git switch -c main
touch README.md
git add README.md
git commit -m "add README"
git push -u origin main
```

